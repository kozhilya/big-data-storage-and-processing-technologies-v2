from MyLibs import LinkedList

print("===== Init =====")

l = LinkedList.LinkedList()
print(l)

print("===== Test 1: Inserting =====")

print("> insert(1, \"Value 1\")")
l.insert(1, "Value 1")
print(l)

print("> insert(2, \"Value 2\")")
l.insert(2, "Value 2")
print(l)

print("> insert(3, \"Value 3\")")
l.insert(3, "Value 3")
print(l)

print("> insert(4, \"Value 4\")")
l.insert(4, "Value 4")
print(l)

print("===== Test 2: Searching =====")

print("> search(2)")
print(l.search(2))

print("> search(10)")
print(l.search(10))

print("> search(2) = \"Value 2 (updated)\"")
node = l.search(2)
node.value = "Value 2 (updated)"
print(l)

print("=== Test 3: Deleting ===")
print("> delete(3)")
l.delete(3)
print(l)

from MyLibs import Stack, Queue, Tree
import random

print("===== STACK ====")
s = Stack.Stack()

s.push("Value 1")
s.push("Value 2")
s.push("Value 3")
print(s)

print("Pop: " + s.pop())
print("Pop: " + s.pop())
print(s)

print("===== QUEUE ====")
q = Queue.Queue()

q.enqueue("Value 1")
q.enqueue("Value 2")
q.enqueue("Value 3")
print(q)

print("Dequeue: " + q.dequeue())
print("Dequeue: " + q.dequeue())
print(q)

print("===== TREE ====")
t = Tree.Tree()

for i in range(1, 10):
    val = random.randrange(100)
    print("Tree.add(" + str(val) + ")")
    t.add(val)
    print(t)

print("Tree.search(44): " + str(t.search(44)))
print("Tree.search(22): " + str(t.search(22)))
print("Tree.search(" + str(val) + "): " + str(t.search(val)))

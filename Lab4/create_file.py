import json
import random
import sys

# 10 Gb = 10737418240 b

json_data = open("settings.json").read()
data = json.loads(json_data)

separators = data["separators"]
separators_chances_sum = 0
words = data["words"].split(' ')

for i in range(len(separators)):
    separators_chances_sum += separators[i]["chance"]

size = 0
lines = 0
file = open(data["block_path"], 'w')

while (size < data["block_size"]):
    word = words[random.randrange(len(words))]
    file.write(word)
    size += len(word)

    separator = "";
    separator_i = random.randrange(separators_chances_sum)
    i = 0
    while separator_i >= 0:
        separator = separators[i]["text"]
        separator_i -= separators[i]["chance"]
        i += 1
    file.write(separator)
    size += len(separator)
    if separator == "\n":
        lines += 1
#    print(str(size) + " / " + str(data["block_size"]) + " (" + str(100 * size / data["block_size"]) + "%)")

file.close()

print("Файл блока текста создан! Размер: " + str(size) + " байт; в файле " + str(lines) + " строк")

# ==================================================
block = open(data["block_path"], 'r').read()

file = open(data["path"], 'w')
file.close()

file = open(data["path"], 'a')
file_size = 0
file_lines = 0
while (file_size < data["size"]):
    file.write(block + "\n")
    file_size += size + 1
    file_lines += lines
file.close()

# while (size < data["block_size"]):

print("Итоговый файл создан! Размер: " + str(file_size) + " байт; в файле " + str(file_lines) + " строк")

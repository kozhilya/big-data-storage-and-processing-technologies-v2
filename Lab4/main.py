import json

json_data = open("settings.json").read()
data = json.loads(json_data)

file = open(data["path"])
line = 1
line_len = 0
block_size = 50000;

transfer = 0

while True:
    buf = file.read(block_size)
    if buf == '':
        break

    last = 0
    while True:
        f = buf.find("\n", last + 1)

        if f == -1:
            transfer = block_size - last
            break

        if (line % data["print_lines"] == 0): # 333667
            print("В строке " + str(line) + " находится " + str(f - last + transfer) + " символов")

        line += 1
        last = f
        transfer = 0

print("Файл обработан!")

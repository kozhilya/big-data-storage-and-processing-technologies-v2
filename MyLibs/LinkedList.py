class LinkedListNode:
    def __init__(self, key, value, prev_node=None, next_node=None):
        self.key = key
        self.value = value
        self.prev_node = prev_node
        self.next_node = next_node

    def getkey(node):
        return str(node.key) if (node is not None) else "None"

    def __str__(self):
        return \
            "[Node (" + str(self.key) + ", " + str(self.value) + "); " + \
            "Prev: " + LinkedListNode.getkey(self.prev_node) + "; " + \
            "Next: " + LinkedListNode.getkey(self.next_node) + \
            "]"


class LinkedList:
    def __init__(self):
        self.first_node = None
        self.count = 0
    def __str__(self):
        result = "[LinkedList: {\r\n"
        current = self.first_node
        i = 0
        while current is not None:
            result += "\t" + str(i) + ": " + current.__str__() + "\r\n"
            current = current.next_node
            i += 1
        return result + "}]"
    def __count__(self):
        c = 0
        current = self.first_node
        while current is not None:
            c, current = c + 1, current.next_node
        self.count = c

    def search(self, key):
        current = self.first_node
        while current is not None:
            if current.key == key:
                return current
            current = current.next_node
        return None

    def insert(self, key, value):
        existed = self.search(key)
        if existed is not None:
            return existed
        new_node, node = LinkedListNode(key, value, None, self.first_node), self.first_node
        self.first_node = new_node
        if node is not None:
            node.prev_node = new_node
        self.__count__()
        return new_node

    def delete(self, key):
        node = self.search(key)
        if node is None:
            return False

        if node.prev_node is not None:
            node.prev_node.next_node = node.next_node
        else:
            self.first_node = node.next_node

        if node.next_node is not None:
            node.next_node.prev_node = node.prev_node

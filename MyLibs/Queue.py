class QueueNode:
    def __init__(self, value, next_node=None):
        self.value = value
        self.next_node = next_node

    def __str__(self):
        return "(" + str(self.value) + ")"

class Queue:
    def __init__(self):
        self.tail_node = None
        self.head_node = None
        self.count = 0

    def __str__(self):
        result = "[Queue: { <- ="
        current = self.head_node
        while current is not None:
            result += current.__str__()
            if current == self.head_node:
                result += "="
            result += " "
            current = current.next_node
        return result + "}]"

    def reset(self):
        self.__init__()

    def __count__(self):
        c = 0
        current = self.tail_node
        while current is not None:
            c, current = c + 1, current.next_node
        self.count = c

    def enqueue(self, value):
        new_node = QueueNode(value, None)
        if self.tail_node is not None:
            self.tail_node.next_node = new_node
        self.tail_node = new_node
        if self.head_node is None:
            self.head_node = self.tail_node
        self.__count__()

    def dequeue(self):
        if self.head_node is None:
            return None;

        val = self.head_node.value
        self.head_node = self.head_node.next_node
        self.__count__()
        return val

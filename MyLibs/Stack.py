class StackNode:
    def __init__(self, value, prev_node=None):
        self.value = value
        self.prev_node = prev_node

    def __str__(self):
        return "(" + str(self.value) + ")"


class Stack:
    def __init__(self):
        self.top_node = None
        self.count = 0

    def __str__(self):
        result = "[Stack: { <- ="
        current = self.top_node
        while current is not None:
            result += current.__str__()
            if current == self.top_node:
                result += "="
            result += " "
            current = current.prev_node
        return result + "}]"

    def reset(self):
        self.__init__()

    def __count__(self):
        c = 0
        current = self.top_node
        while current is not None:
            c, current = c + 1, current.prev_node
        self.count = c

    def push(self, value):
        self.top_node = StackNode(value, self.top_node)
        self.__count__()

    def pop(self):
        if self.top_node is None:
            return None;

        val = self.top_node.value
        self.top_node = self.top_node.prev_node
        self.__count__()
        return val

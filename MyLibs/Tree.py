class TreeNode:
    def __init__(self, value, parent_node, right_child=None, left_child=None):
        self.value = value
        self.parent_node = parent_node
        self.right_child = right_child
        self.left_child = left_child

    def __str__(self):
        return TreeNode.dump(self)

    def dump(node, tabs=0, prefix=""):
        if node is None:
            return ('\t' * tabs) + "None"
        return ('\t' * tabs) + ("[" + prefix + "] " if prefix != "" else "") + str(node.value) + \
               ("\r\n" + TreeNode.dump(node.left_child, tabs + 1, "l") if node.left_child is not None else "") + \
               ("\r\n" + TreeNode.dump(node.right_child, tabs + 1, "r") if node.right_child is not None else "")

    # TODO: do it balanced tree???
    def add(node, value):
        el = TreeNode(value, node)
        if node is None:
            return el
        elif node.left_child is None:
            node.left_child = el
        elif node.right_child is None:
            node.right_child = el
        else:
            if value <= node.value:
                node.left_child.add(value)
            else:
                node.right_child.add(value)
        return node

    def search(node, value):
        if node is None:
            return False
        if value == node.value:
            return True

        return TreeNode.search(node.left_child, value) or TreeNode.search(node.right_child, value)


class Tree:
    def __init__(self):
        self.root = None

    def __str__(self):
        return TreeNode.dump(self.root, 0)

    def add(self, value):
        self.root = TreeNode.add(self.root, value)

    def search(self, value):
        return TreeNode.search(self.root, value)
